'use strict';
var BaseClient = require('./base-client');
var _ = require('lodash');


function CatalogClient(options) {
  BaseClient.call(this);
  this.catalogBase = '/product-catalog';
}

CatalogClient.prototype = Object.create(BaseClient.prototype);

CatalogClient.prototype.getTaxonomy =  function() {
  var url = this.catalogBase + '/taxonomy';
  var options = {
    url: url
  }
  return this._get(options);
}

CatalogClient.prototype.getProducts = function(data) {
  var url = this.catalogBase + '/products/search?filter_path=hits.hits._source&size=10';
  var options = {
    url: url,
    data: data
  }
  return this._post(options)
}

CatalogClient.prototype.getProductDetails = function(id) {
  var url = this.catalogBase + '/products/'+id;
  var options = {
    url: url
  }
  return this._get(options);
}

module.exports = CatalogClient;
