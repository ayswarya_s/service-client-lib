'use strict';
var Axios = require('axios');
var _ = require('lodash')

function BaseClient(options) {

  this.baseOptions = _.merge(options, {proxy: '127.0.0.1', port: 443, protocol: 'https'});
}

BaseClient.prototype._get = function _get(options) {
  var me = this;

  var o = _.extend({}, me.baseOptions, options);
  var headers = _.extend({},
    {
      'x-ecom-app-id': 'AuthToken'
    }, o.headers
  )
  return new Promise(function(resolve, reject) {
    Axios.get(o.url, {headers: headers, params: o.params, proxy: {proxy: '127.0.0.1', port: 3000}})
    .then(function(json) {
      resolve(json.data);
    })
    .catch(function(response) {
      reject(response);
    });
  });
}

BaseClient.prototype._post = function _post(options) {
  var me = this;
  var o = _.extend({}, me.baseOptions, options)
  var headers = _.extend({},
    {
      'x-ecom-app-id': 'AuthToken'
    }, o.headers
  )

  return new Promise(function(resolve, reject) {
    Axios.post(o.url, o.data, {headers: headers})
    .then(function(json) {
      resolve(json.data);
    })
    .catch(function(response) {
      reject(response);
    });
  })
}

BaseClient.prototype._put = function _put(options) {
  var me = this;
  var o = _.extend({}, me.baseOptions, options)
  return new Promise(function(resolve, reject) {
    Axios.create({
      url: o.url,
      method: 'put',
      headers: _.extend({}, {
        'x-ecom-app-id': 'AuthToken'
      }, o.headers),
      params: o.params,
      data: o.data,
      responseType: 'json'
    })
    .then(function(json) {
      resolve(json.data);
    })
    .catch(function(response) {
      reject(response);
    });
  })
}

BaseClient.prototype._del = function _del(options) {
  var me = this;
  var o = _.extend({}, me.baseOptions, options)
  return new Promise(function(resolve, reject) {
    Axios.create({
      url: o.url,
      method: 'devare',
      headers: _.extend({}, {
        'x-ecom-app-id': 'AuthToken'
      }, o.headers),
      responseType: 'json'
    })
    .then(function(json) {
      resolve(json.data);
    })
    .catch(function(response) {
      reject(response);
    });
  })
}

BaseClient.prototype._toQueryString = function _toQueryString(obj) {
  return _.map(obj, function(v, k){
    return encodeURIComponent(k) + '=' + encodeURIComponent(v);
  }).join('&');
};

module.exports = BaseClient;
